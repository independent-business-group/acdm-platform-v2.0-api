<?php
namespace App\Core\Domain\Contracts;


use App\Core\Domain\Contracts\Pagination\ListSearchParameter;
use App\Core\Domain\Contracts\Pagination\ListSearchResult;
use App\Core\Domain\Contracts\Pagination\PageSearchParameter;
use App\Core\Domain\Contracts\Pagination\PageSearchResult;
use App\Infrastructure\Persistence\Eloquents\BaseEloquent;
use Illuminate\Support\Collection;

interface IRepository extends IUnitOfWorkRepository
{
    public function newInstance(array $attributes = null): BaseEloquent;

    public function all(): Collection;

    public function allWithChunk(): Collection;

    public function lists(int $limit = 10, int $offset = 0, array $columns = ['*']): Collection;

    public function get(int $id): BaseEloquent;

    public function find(int $id, array $columns = ['*']): BaseEloquent;

    public function findWhere(array $where, array $columns = ['*']): Collection;

    public function firstOrCreate(array $attributes): BaseEloquent;

    public function firstOrNull(array $attributes);

    public function deleteWhere(array $where): bool;

    public function count(): int;

    public function listSearch(ListSearchParameter $parameter, array $columns = ['*']): ListSearchResult;

    public function listSearchWithChunk(ListSearchParameter $parameter, array $columns = ['*']): ListSearchResult;

    public function pageSearch(PageSearchParameter $parameter, array $columns = ['*']): PageSearchResult;
}
